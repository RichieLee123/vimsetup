#! /bin/bash

set -e

echo 'preparing vim setup'

read -p "please enter an install folder for vim config(default: ~/vimsetup) " userInput

FILEPATH=${userInput:-"~/vimsetup"}
RESTOREPATH="~/VimBackup"

if [[ -d $FILEPATH ]]; then
    echo "$FILEPATH is not a valid directory, stopping."
    exit 1
fi

if [[ -e "~/.vimrc" ]]; then
    echo "backing up existing config to $RESTOREPATH"
    cp -f -v ~/.vimrc "$RESTOREPATH/.vimrc"
else
    echo "nothing found at ~/.vimrc skipping backup"
fi

git clone https://RichieLee123@bitbucket.org/RichieLee123/vimsetup.git $FILEPATH

echo "creating symlinks from $FILEPATH to vim dotfiles"
ln -s -f $FILEPATH/.vim_runtime ~/.vim_runtime
ln -s -f $FILEPATH/.vimrc ~/.vimrc

echo "Installing nodeJs for plug"
curl -sL install-node.now.sh/lts | bash

echo "Installing vim-plug"
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "running PlugInstall"
vim +'PlugInstall --sync' +qa

echo "Done, Enjoy!"
exit 0;
